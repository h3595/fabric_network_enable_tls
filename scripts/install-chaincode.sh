
CC_NAME="mycc"
VERSION=1
SEQUENCE=1

setup(){
    export PATH=${PWD}/bin/:$PATH
    export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin

    pushd ./chaincode || exit
    go mod init ${CC_NAME}.go
    go mod tidy
    GO111MODULE=on go mod vendor
    popd || exit
}

setup

peer lifecycle chaincode package ${CC_NAME}.tar.gz --path ./chaincode --lang golang --label ${CC_NAME}_1

installChaincodeOrg1(){

    export CORE_PEER_MSPCONFIGPATH=${PWD}/crypto-config/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp

    export CORE_PEER_LOCALMSPID="Org1MSP"
    export CORE_PEER_MSPID="Org1MSP"
    export CORE_PEER_ADDRESS=localhost:7051

    peer lifecycle chaincode install ${CC_NAME}.tar.gz

    peer lifecycle chaincode queryinstalled

    peer lifecycle chaincode queryinstalled >&log.txt
    cat log.txt
    CC_PACKAGE_ID=$(sed -n "/${CC_NAME}_${VERSION}/{s/^Package ID: //; s/, Label:.*$//;p;}" log.txt )

    peer lifecycle chaincode approveformyorg --channelID mychannel --name ${CC_NAME} --version ${VERSION} --init-required --package-id $CC_PACKAGE_ID --sequence ${SEQUENCE}

    peer lifecycle chaincode checkcommitreadiness --channelID mychannel --name ${CC_NAME} --version ${VERSION} --sequence ${SEQUENCE} --output json --init-required

}
installChaincodeOrg1

installChaincodeOrg2(){

    export CORE_PEER_MSPCONFIGPATH=${PWD}/crypto-config/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp

    export CORE_PEER_LOCALMSPID="Org2MSP"
    export CORE_PEER_MSPID="Org2MSP"
    export CORE_PEER_ADDRESS=localhost:8052

    peer lifecycle chaincode install ${CC_NAME}.tar.gz

    peer lifecycle chaincode queryinstalled

    peer lifecycle chaincode queryinstalled >&log.txt
    cat log.txt
    CC_PACKAGE_ID=$(sed -n "/${CC_NAME}_${VERSION}/{s/^Package ID: //; s/, Label:.*$//;p;}" log.txt )

    peer lifecycle chaincode approveformyorg --channelID mychannel --name ${CC_NAME} --version ${VERSION} --init-required --package-id $CC_PACKAGE_ID --sequence ${SEQUENCE}

    peer lifecycle chaincode checkcommitreadiness --channelID mychannel --name ${CC_NAME} --version ${VERSION} --sequence ${SEQUENCE} --output json --init-required

}

installChaincodeOrg2

sleep 3s

peer lifecycle chaincode commit -o localhost:7050 --channelID mychannel --name ${CC_NAME} --version ${VERSION} --sequence ${SEQUENCE} --init-required --peerAddresses localhost:7051 --peerAddresses localhost:8052

sleep 3s

peer chaincode invoke -o localhost:7050 --isInit -C mychannel -n ${CC_NAME} --peerAddresses localhost:7051 --peerAddresses localhost:8052 -c '{"Args":["InitLedger",""]}' --waitForEvent 

sleep 3s

peer chaincode query -C mychannel -n ${CC_NAME} -c '{"Args":["GetAllAssets","",""]}'

# peer chaincode invoke -o localhost:7050 -C mychannel -n ${CC_NAME} --peerAddresses localhost:7051 --peerAddresses localhost:8052  -c '{"function":"CreateAsset","Args":["asset7","GREEN","7","REACH","77"]}'

# peer chaincode query -C mychannel -n ${CC_NAME} -c '{"Args":["ReadAsset","asset7"]}'

# peer chaincode invoke -o localhost:7050 -C mychannel -n ${CC_NAME} --peerAddresses localhost:7051 --peerAddresses localhost:8052 -c '{"function":"UpdateAsset","Args":["asset7","GREEN","7","NOU REACH","777"]}'

# peer chaincode invoke -o localhost:7050 -C mychannel -n ${CC_NAME} --peerAddresses localhost:7051 --peerAddresses localhost:8052 -c '{"function":"DeleteAsset","Args":["asset6"]}'